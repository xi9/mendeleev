# `Mendeleev`

...is a package for element and nuclide informatics in Rust.

It also contains an `Atom` struct, which can `ionize`, `decay`, etc.

## References

All of the data which is provided comes from a few places:

- The [National Nuclear Data Center](https://www.nndc.bnl.gov/)'s [Evaluated Nuclear Structure Data File](https://www.nndc.bnl.gov/ensdfarchivals/)
- The [National Institute of Standards and Technology](https://www.nist.gov/)'s [
Atomic Weights and Isotopic Compositions with Relative Atomic Masses
](https://www.nist.gov/pml/atomic-weights-and-isotopic-compositions-relative-atomic-masses)
